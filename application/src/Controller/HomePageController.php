<?php

namespace App\Controller;

use App\Entity\Software;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{
    #[Route('/', name: 'home_page')]
    public function index(ManagerRegistry $manager): Response
    {
        $softwareRepository = $manager->getRepository(Software::class);

        return $this->render('home_page/index.html.twig', [
            'softwares' => $softwareRepository->findBy(['pickOfTheMonth' => true]),
        ]);
    }
}
