<?php

namespace App\Tests\Controller;

use Adrien\FixturesForTests\FixtureAttachedTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomePageTest extends WebTestCase
{
    use FixtureAttachedTrait;

    /**
     * selection of the month on homepage.
     */
    public function testSoftwareListing(): void
    {
        self::ensureKernelShutdown();
        $client = static::createClient();

        // 1. Se rendre sur la page d'accueil
        $crawler = $client->request('GET', '/');

        /*  2. Vérifier que la sélection du mois :*/
        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('.app_pick-of-the-month h2', 'La sélection du mois');

        self::assertCount(4, $client->getCrawler()->filter('.app_pick-of-the-month li'));

        self::assertSelectorTextContains('.app_pick-of-the-month li:nth-child(1)', 'OpenADS');
        self::assertSelectorTextContains('.app_pick-of-the-month li:nth-child(2)', 'Publik');
        self::assertSelectorTextContains('.app_pick-of-the-month li:nth-child(3)', 'OCS Inventory NG');
        self::assertSelectorTextContains('.app_pick-of-the-month li:nth-child(4)', 'Départements et Notaires');
    }
}
