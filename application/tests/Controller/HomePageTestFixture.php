<?php

namespace App\Tests\Controller;

use App\Entity\Software;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Persistence\ObjectManager;

class HomePageTestFixture extends AbstractFixture
{
    public function load(ObjectManager $manager)
    {
        $software = $this->makeSoftware('OpenADS');
        $software->setPickOfTheMonth(true);
        $this->addReference('first_picked_software', $software);
        $manager->persist($software);

        $software = $this->makeSoftware('Publik');
        $software->setPickOfTheMonth(true);
        $manager->persist($software);

        $software = $this->makeSoftware('OCS Inventory NG');
        $software->setPickOfTheMonth(true);
        $manager->persist($software);

        $software = $this->makeSoftware('Départements et Notaires');
        $software->setPickOfTheMonth(true);
        $manager->persist($software);

        $software = $this->makeSoftware('Lorem');
        $manager->persist($software);

        $software = $this->makeSoftware('Ipsum');
        $manager->persist($software);

        $manager->flush();

//        * OpenADS
//        * Publik
//        * OCS Inventory NG
//    * Départements et Notaires
    }

    private function makeSoftware(string $name = ''): Software
    {
        $software = new Software();
        $software->setName($name);
        $software->setLogo('');
        $software->setDescription('...');

        return $software;
    }
}
